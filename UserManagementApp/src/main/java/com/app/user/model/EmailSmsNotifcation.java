package com.app.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.app.user.util.BaseEntity;

@Entity
@Table(name="email_sms_notification")
public class EmailSmsNotifcation extends BaseEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	private String toEmail;
	
	private String subject;
	
	private String ccEmail;
	
	private String emailBody;
	

	private enum EmailStatus {
		New,
		InProgress,
		Sent,
		Error
	}
	
	
	@Enumerated(EnumType.STRING)
	private EmailStatus emailStatus;
	
	

}
