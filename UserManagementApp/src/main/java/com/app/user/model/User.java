package com.app.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.envers.Audited;

import com.app.user.util.BaseEntity;

import lombok.Data;

@Entity
@Table(name="user")
@Data
@Audited
public class User extends BaseEntity{
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="id")
	private Long id;
	
	private String username;
	
	private String firstName;
	
	private String middleName;
	
	private String lastName;
	
	private String email;
	
	private Long phoneNumber;

}
