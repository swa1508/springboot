package com.app.user.util;

import java.time.LocalDateTime;

import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import org.hibernate.envers.Audited;

@MappedSuperclass
@Audited
public class BaseEntity {
	
	private LocalDateTime createdTime;
	
	private LocalDateTime modifiedTime;

	public LocalDateTime getCreatedTime() {
		return createdTime;
	}

	public void setCreatedTime(LocalDateTime createdTime) {
		this.createdTime = createdTime;
	}

	public LocalDateTime getModifiedTime() {
		return modifiedTime;
	}

	public void setModifiedTime(LocalDateTime modifiedTime) {
		this.modifiedTime = modifiedTime;
	}
	
	@PrePersist
    protected void prePersist() {
        if (this.createdTime == null) createdTime =  LocalDateTime.now();
        if (this.modifiedTime == null) modifiedTime = LocalDateTime.now();
    }

    @PreUpdate
    protected void preUpdate() {
        this.modifiedTime = LocalDateTime.now();
    }
	
}
