package com.app.user.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.user.model.User;
import com.app.user.repository.UserRepository;
import com.app.user.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	@Autowired
	private UserRepository userRpository;
	
	@Override
	public User create(User user) {
		return userRpository.save(user);
	}

	@Override
	public List<User> getAllUsers() {
		return userRpository.findAll();
	}

	@Override
	public User update(Long id, User user) {
		User dbUser  = userRpository.findById(id).get();
		dbUser.setId(id);
		dbUser.setUsername(user.getUsername());
		dbUser.setFirstName(user.getFirstName());
		dbUser.setMiddleName(user.getMiddleName());
		dbUser.setLastName(user.getLastName());
		dbUser.setEmail(user.getEmail());
		dbUser.setPhoneNumber(user.getPhoneNumber());
		return userRpository.save(dbUser);
	}

}
