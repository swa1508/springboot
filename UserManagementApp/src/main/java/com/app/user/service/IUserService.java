package com.app.user.service;

import java.util.List;

import com.app.user.model.User;

public interface IUserService {

	User create(User user);

	List<User> getAllUsers();

	User update(Long id, User user);

}
