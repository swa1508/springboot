package com.app.user.rest;

import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.app.user.model.User;

@RequestMapping("/user")
public interface IUserRest {
	
	@GetMapping("/hello")
	public String helloMesage();

	//1. Create User
	@PostMapping("/create")
	public User create(@RequestBody User user);
	
	//2. Select All Users
	@GetMapping("/getAll")
	public List<User> getAllUsers();
	
	//3. Update User
	@PostMapping("/update/{id}")
	public User update(@PathVariable("id") Long id ,@RequestBody User user);
	
	//4. Delete User by Id.
	
	//5. Delete All User.
	
	//6. Find User By Id.
	
	//7. import User
	
	//8. export User
	
	//9. upload User Documents
	
	
}
