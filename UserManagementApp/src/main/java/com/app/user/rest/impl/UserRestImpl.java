package com.app.user.rest.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import com.app.user.model.User;
import com.app.user.rest.IUserRest;
import com.app.user.service.IUserService;

@RestController
public class UserRestImpl implements IUserRest {
	
	@Autowired
	private IUserService userService;
	

	@Override
	public String helloMesage() {
		return "Hello from User Management Application";
	}

	@Override
	public User create(User user) {
		return userService.create(user);
	}

	@Override
	public List<User> getAllUsers() {
		return userService.getAllUsers();
	}

	@Override
	public User update(Long id,User user) {
		System.out.println("id : "+id);
		System.out.println("user : "+user);
		return userService.update(id,user);
	}

}
