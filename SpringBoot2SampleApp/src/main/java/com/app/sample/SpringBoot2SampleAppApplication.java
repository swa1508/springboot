package com.app.sample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBoot2SampleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBoot2SampleAppApplication.class, args);
	}

}
