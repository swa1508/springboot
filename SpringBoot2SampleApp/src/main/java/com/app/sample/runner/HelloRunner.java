package com.app.sample.runner;

import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
@Component
public class HelloRunner implements CommandLineRunner {

	@Override
	public void run(String... args) throws Exception {
		System.out.println(this.getClass().getName());
		System.out.println("Hello From Hello Runner");

	}

}
